package utfpr.ct.dainf.if62c.pratica;

/**
 * @author Marcelo Guimarães da Costa <marcelo@unicode.com.br> 
 */
public class MatrizInvalidaException extends Exception{
    private final int num_linhas;
    private final int num_colunas;

    public MatrizInvalidaException(int num_linhas, int num_colunas){
        super(String.format(
                "Matriz %dx%d não pode ser criada", 
                num_linhas, num_colunas));

        this.num_linhas = num_linhas;
        this.num_colunas = num_colunas;
    }

    public int getNumLinhas() {
        return num_linhas;
    }

    public int getNumColunas() {
        return num_colunas;
    }
}
