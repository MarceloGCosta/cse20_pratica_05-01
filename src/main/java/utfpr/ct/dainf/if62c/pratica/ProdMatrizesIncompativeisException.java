package utfpr.ct.dainf.if62c.pratica;

/**
 * @author Marcelo Guimarães da Costa <marcelo@unicode.com.br> 
 */
public class ProdMatrizesIncompativeisException extends MatrizesIncompativeisException{
    public ProdMatrizesIncompativeisException(Matriz m1, Matriz m2){
        super(m1,m2,String.format(
                "Matrizes de %dx%d e %dx%d não podem ser multiplicadas",
                m1.getMatriz().length, m1.getMatriz()[0].length,
                m2.getMatriz().length, m2.getMatriz()[0].length));
    }
}
